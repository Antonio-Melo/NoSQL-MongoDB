package logic;

public class Message {
    private User sender;
    private User receiver;
    private String type;
    private String message;

    public Message(User sender, User receiver, String type, String message){
        this.sender = sender;
        this.receiver = receiver;
        this.type = type;
        this. message = message;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}

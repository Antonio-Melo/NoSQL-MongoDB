package logic;
import java.lang.String;
import java.util.UUID;

public class User  {
	
	private UUID idUser;
	private String username;
	private String password;
	private String eMail;
	private boolean banned;

	public UUID getIdUser() {
		return idUser;
	}

	public void setIdUser(UUID idUser) {
		this.idUser = idUser;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	private boolean admin;

	public String geteMail() {
		return eMail;
	}

	public User (String username, String passwd, String eMail,boolean admin) {
		
		this.idUser = UUID.randomUUID();
		this.username = username;
		this.password = passwd;
		this.eMail   =  eMail;
		this.banned = false;
		this.admin = admin;
	
	}
	public String getUsername() {
		return username;
	}
	public String getEmail() {
		return eMail;
	}
	public String getPassword() {
		return password;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setEmail(String eMail) {
		if (eMail.contains("@")) 
			this.eMail = eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public int hashCode() {
		
		final int numPrime = 31;
    	int result = 1;
    	result = numPrime*result + ((idUser == null) ? 0 : idUser.hashCode());
    	return result;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		boolean res = false;
    	User aux;
    	if(otherObject!=null)
    		if(otherObject instanceof User){
    			aux = (User) otherObject;
    			res = this.idUser.equals(aux.idUser);
    					
    		}
    	return res;
	}
}

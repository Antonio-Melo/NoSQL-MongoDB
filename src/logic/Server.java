package logic;


import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import java.util.Arrays;
import com.mongodb.Block;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private MongoClient client;
    private MongoDatabase database;

    public Server(){
        connectToDatabase();
        checkingCollections();
    }

    public void connectToDatabase(){
        System.out.print("Connecting with database...");
        client = new MongoClient();
        database = client.getDatabase("TestDB");
        System.out.println("Done");
    }

    public void checkingCollections(){
        if(!collectionExists(database, "Users")){
            System.out.println("Users collection does not exist...Creating one!");
            createCollection("Users");
        }else System.out.println("Users collection exists");

        if(!collectionExists(database, "Messages")){
            System.out.println("Messages collection does not exist...Creating one!");
            createCollection("Messages");
        }else System.out.println("Messages collection exists");
    }

    public void createCollection(String collectionName){
        database.createCollection(collectionName);
    }

    public static boolean collectionExists(MongoDatabase db, String collectionName)
    {
        for (String name : db.listCollectionNames())
        {
            if(name.equals(collectionName)) return true;
        }
        return false;
    }
    //USERS----------------------------------------------------------------------------
    public void addUser(User user){
        if(!checkIfUserAlreadyExists(user)){
            MongoCollection<Document> collection = database.getCollection("Users");
            Document doc = new Document("username", user.getUsername())
                    .append("password", user.getPassword())
                    .append("email", user.getEmail());
            collection.insertOne(doc);
        }
    }

    public boolean checkIfUserAlreadyExists(User user){
        MongoCollection<Document> collection = database.getCollection("Users");
        FindIterable<Document> iterable = collection.find(eq("username", user.getUsername()));
        if(iterable.first() != null) return true;
        else return false;
    }

    public void editAccount(User user){
        MongoCollection<Document> collection = database.getCollection("Users");
        if(checkIfUserAlreadyExists(user)){
            Document doc = new Document("username", user.getUsername())
                    .append("password", user.getPassword())
                    .append("email", user.getEmail());
            collection.updateOne(eq("username", user.getUsername()),new Document("$set",doc));
        }
    }

    public void removeUser(User user){
        MongoCollection<Document> collection = database.getCollection("Users");
        collection.deleteOne(eq("username",user.getUsername()));
    }
    //MESSAGES-------------------------------------------------------------------------
    public  void addMessage(Message msg){
        MongoCollection<Document> collection = database.getCollection("Messages");
        Document doc = new Document("sender", msg.getSender().getUsername())
                .append("receiver", msg.getReceiver().getUsername())
                .append("type", msg.getType())
                .append("message", msg.getMessage());
        collection.insertOne(doc);
    }

    public void deleteMessage (Message msg){
        MongoCollection<Document> collection = database.getCollection("Messages");
        collection.deleteOne(
                and(
                        eq("sender",msg.getSender().getUsername()),
                        eq("receiver",msg.getReceiver().getUsername()),
                        eq("type",msg.getType()),
                        eq("message", msg.getMessage())
                )
        );
    }

    //ADMIN --------------------------------------------------------------------
    public void banUser(User admin, User user){
        if(admin.isAdmin()) user.setBanned(true);
    }

    public void unBanUser(User admin, User user){
        if(admin.isAdmin()) user.setBanned(false);
    }
}
